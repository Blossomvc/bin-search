#include "catch.hpp"
#include "../src/search.hpp"
#include <vector>
#include <string>


using namespace std;



TEST_CASE("TEST TO PASS", "[test1]")
{
  vector<int> D1 = {1, 2, 3, 4, 5};

  vector<char> D2 = {'a', '2', '/', '@', '$'};//not sorted

  vector<string> D3 = {"same","shame", "tame", "theme", "umbrella"};

  CHECK( binarySearch(D1 , 5) == 4); 
  CHECK( binarySearch(D1 , 4) == 3); 
  CHECK( binarySearch(D2 , '@') == 3); //see ascii
  CHECK( binarySearch(D1 , 7) == -1); 
  CHECK( binarySearch(D3 , "theme") == 3); 
  CHECK( binarySearch(D3 , "umbrella") == 4); 

}

